package ku.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An array that is an object have a method that can show whether the file is
 * exist or not.
 * 
 * @author Narut Poovorakit
 * @version 20.02.2015
 *
 * @param <T>
 *            an object
 */
public class ArrayIterator<T> implements Iterator {
	// create an object array.
	private T[] array;
	private int cursor = 0;

	/**
	 * Initialize a constructor.
	 * 
	 * @param array
	 *            of an object.
	 */
	public ArrayIterator(T[] array) {
		this.array = array;
	}

	/**
	 * Method check if the next file in array is exist.
	 */
	public boolean hasNext() {
		for (int i = cursor; i < array.length; i++) {
			if (array[i] != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * if the file is exist return that file.
	 */
	public T next() {
		T object = null;
		if (hasNext()) {
			while (array[cursor] == null) {
				cursor++;
			}
			object = array[cursor];
			cursor++;

		} else {
			throw new NoSuchElementException();
		}
		return object;
	}

	/**
	 * remove the file in the array.
	 */
	public void remove() {
		array[cursor - 1] = null;
	}

}
