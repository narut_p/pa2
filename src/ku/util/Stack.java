package ku.util;

import java.util.EmptyStackException;

/**
 * A class that show the solution of the stack to peek, pop and push.
 * 
 * @author Narut Poovorakit
 * @version 20.02.2015
 * @param <T>
 *            an object
 */
public class Stack<T> {
	// create an object array.
	private T[] items;
	private int capacity;

	/**
	 * Initialize a constructor.
	 * 
	 * @param capacity
	 *            is maximum number of the item array.
	 */
	public Stack(int capacity) {
		if (capacity < 0)
			capacity = 0;
		this.capacity = capacity;
		items = (T[]) new Object[capacity];
	}

	/**
	 * Capacity method.
	 * 
	 * @return the capacity. If have no capacity it's return -1.
	 */
	public int capacity() {
		if (capacity >= 0)
			return capacity;
		return -1;
	}

	/**
	 * 
	 * @return true if size is equal to zero.
	 */
	public boolean isEmpty() {
		if (size() == 0)
			return true;
		return false;
	}

	/**
	 * 
	 * @return true if the size is equal to the capacity.
	 */
	public boolean isFull() {
		if (size() == capacity) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return nothing if it not have anything to peek.
	 * 
	 */
	public T peek() {
		if (size() == 0)
			return null;
		return items[this.size() - 1];
	}

	/**
	 * 
	 * @return the top of the stack.
	 */
	public T pop() {
		if (size() == 0)
			throw new EmptyStackException();
		T store = items[this.size() - 1];
		items[this.size() - 1] = null;
		return store;
	}

	/**
	 * method that add the item in the item list.
	 * 
	 * @param obj
	 *            is an object.
	 */
	public void push(T obj) {
		if (obj == null) {
			throw new IllegalArgumentException();
		}
		int index = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				index++;
			}
		}
		if (!isFull()) {
			items[index] = obj;
		}
	}

	/**
	 * 
	 * @return the size of the item list.
	 */
	public int size() {
		int index = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				index++;
			}
		}
		return index;
	}
}
